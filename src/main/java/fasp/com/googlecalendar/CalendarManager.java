package fasp.com.googlecalendar;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.CalendarListEntry;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;

public class CalendarManager {
	private static final String APPLICATION_NAME = "Google Calendar API Java Quickstart";
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static final String TOKENS_DIRECTORY_PATH = "tokens";

	/**
	 * Global instance of the scopes required by this quickstart. If modifying these
	 * scopes, delete your previously saved tokens/ folder.
	 */
	private static final List<String> SCOPES = Arrays.asList(CalendarScopes.CALENDAR_READONLY,
			CalendarScopes.CALENDAR_EVENTS);
	// 	Save your downloaded credentials file to src/main/resources
	private static final String CREDENTIALS_FILE_PATH = "/credentials.json";
	final static Set<String> KEYWORDS = new HashSet<String>(
			Arrays.asList("birthday", "geburtstag", "anniversary", "saint"));
	private static Calendar service;

	/**
	 * To fill these two fields, use listAllCalenders() and the respective field
	 * 'id' from the resulting json
	 */
	private static final String SOURCE_CALENDAR = "";
	private static final String TARGET_CALENDAR = "";

	/**
	 * Creates an authorized Credential object.
	 * 
	 * @param HTTP_TRANSPORT The network HTTP Transport.
	 * @return An authorized Credential object.
	 * @throws IOException If the credentials.json file cannot be found.
	 */
	private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
		// Load client secrets.
		InputStream in = CalendarManager.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				clientSecrets, SCOPES)
						.setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
						.setAccessType("offline").build();
		LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
		return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
	}

	public static void main(String... args) throws IOException, GeneralSecurityException {
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		service = new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
				.setApplicationName(APPLICATION_NAME).build();

		List<Event> events = getEvents(SOURCE_CALENDAR);
		listAllCalenders();
		moveBirthdaysToBirthdayCalendar(events, SOURCE_CALENDAR, TARGET_CALENDAR);

		showUpcomingEvents(events);
	}

	private static List<Event> getEvents(String calendarID) throws IOException {
		List<Event> events = service.events().list(calendarID)// .list("primary")
				// .setMaxResults(10)
				.setMaxResults(null).setTimeMin(new DateTime(System.currentTimeMillis()))
//	                .setOrderBy("startTime")
//	                .setSingleEvents(true)
				.execute().getItems();
		return events;
	}

	private static void showUpcomingEvents(List<Event> events) {
		if (events.isEmpty()) {
			System.out.println("No upcoming events found.");
		} else {
			System.out.println("Upcoming events");
			for (Event event : events) {
				DateTime start = event.getStart().getDateTime();
				if (start == null) {
					start = event.getStart().getDate();
				}
				System.out.printf("%s (%s) (%s)\n", event.getSummary(), start, event.getEtag());
			}
		}
	}

	private static void listAllCalenders() throws IOException {
		HashMap<String, String> myCalendars = new HashMap<String, String>();
		final com.google.api.services.calendar.model.CalendarList list = service.calendarList().list().execute();
		for (CalendarListEntry cal : list.getItems()) {
			System.out.printf("%s, %s\n", cal.getSummary(), cal.getId());
			myCalendars.put(cal.getSummary(), cal.getId());
		}
	}

	private static void moveBirthdaysToBirthdayCalendar(List<Event> items, String sourceCalendar, String targetCalendar)
			throws IOException {
		items.stream().filter(e -> null != e.getSummary()).forEach(e -> move(e, sourceCalendar, targetCalendar));
	}

	private static void move(Event event, String sourceCalendar, String targetCalendar) {
		String lowerCaseSummary = event.getSummary().toLowerCase();

		if (KEYWORDS.stream().anyMatch(s -> lowerCaseSummary.contains(s))) {
			Event newEvent = buildNewRecurringEvent(event);
			Event updatedEvent = null;
			try {
				updatedEvent = service.events().insert(targetCalendar, newEvent).execute();
				service.events().delete(sourceCalendar, event.getId()).execute();
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.printf("%s (%s) \n", updatedEvent.getSummary(), updatedEvent.getStart());
		}
	}

	private static Event buildNewRecurringEvent(Event event) {
		Event newEvent = new Event().setSummary(event.getSummary());
		EventDateTime start = event.getStart();
		start.setTimeZone(event.getStart().getTimeZone());
		EventDateTime end = event.getEnd();
		end.setTimeZone(event.getStart().getTimeZone());
		newEvent.setStart(start);
		newEvent.setEnd(end);
		String[] recurrence = new String[] { "RRULE:FREQ=YEARLY" };
		newEvent.setRecurrence(Arrays.asList(recurrence));
		return newEvent;
	}
}